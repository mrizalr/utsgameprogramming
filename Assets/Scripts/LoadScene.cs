﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    public void loadSceneBtn(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }

    public void OpenPanel(GameObject panel)
    {
        panel.SetActive(true);
    }

    public void ClosePanel(GameObject panel)
    {
        panel.SetActive(false);
    }

    public void ExitBtn()
    {
        Debug.Log("Exit !!");
        Application.Quit(0);
    }
}

